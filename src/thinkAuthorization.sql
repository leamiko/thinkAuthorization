-- 权限组 --
DROP TABLE IF EXISTS `think_auth_group`;
CREATE TABLE `think_auth_group` (
  `ag_id` int(11) NOT NULL AUTO_INCREMENT,
  `ag_name` varchar(255) DEFAULT NULL COMMENT '权限组名',
  `ag_login` varchar(255) DEFAULT NULL COMMENT '默认登陆地址',
  `ag_auth` text COMMENT '权限',
  PRIMARY KEY (`ag_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='权限_用户组列表';

-- 权限组：初始数据 --
INSERT INTO `think_auth_group` VALUES (1,'管理员','/Admin','');
INSERT INTO `think_auth_group` VALUES (2,'会员','/Home','');

-- 模块列表 --
DROP TABLE IF EXISTS `think_auth_module`;
CREATE TABLE `think_auth_module` (
  `am_id` int(11) NOT NULL AUTO_INCREMENT,
  `am_name` varchar(255) DEFAULT NULL COMMENT '模块名称',
  `am_path` varchar(255) DEFAULT NULL COMMENT '模块路径',
  `am_error` tinyint(3) DEFAULT '0' COMMENT '如果扫描时不存在，设置为1',
  PRIMARY KEY (`am_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='权限_模块列表';

-- 模块列表：初始数据 --
INSERT INTO `think_auth_module` VALUES (1,'前台模块','Home',0);
INSERT INTO `think_auth_module` VALUES (2,'管理员后台','Admin',0);

-- Controller --
DROP TABLE IF EXISTS `think_auth_controller`;
CREATE TABLE `think_auth_controller` (
  `ac_id` int(11) NOT NULL AUTO_INCREMENT,
  `ac_name` varchar(255) DEFAULT NULL COMMENT '控制器名',
  `ac_path` varchar(255) DEFAULT NULL,
  `ac_mid` int(11) DEFAULT NULL COMMENT '模块编号',
  `ac_error` tinyint(3) DEFAULT '0' COMMENT '如果扫描时不存在，设置为1',
  PRIMARY KEY (`ac_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='权限_控制器列表';

-- Action --
DROP TABLE IF EXISTS `think_auth_action`;
CREATE TABLE `think_auth_action` (
  `aa_id` int(11) NOT NULL AUTO_INCREMENT,
  `aa_name` varchar(255) DEFAULT NULL COMMENT '功能名',
  `aa_path` varchar(255) DEFAULT NULL COMMENT '功能Action名',
  `aa_key` varchar(255) DEFAULT NULL COMMENT '权限值',
  `aa_cid` int(11) DEFAULT NULL COMMENT '控制器编号',
  `aa_error` tinyint(3) DEFAULT '0' COMMENT '如果扫描时不存在，设置为1',
  PRIMARY KEY (`aa_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='权限_功能列表';
